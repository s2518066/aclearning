import numpy as np
import matplotlib.pyplot as plt
f = open("output", 'w')
f.write("Name, \t\t\t\t\t\tEpisodes,AvgDist,MaxDist, AvgSpeed,\tStd\n")


def clipDist(i): 
    if (i < 0.5):
        return i
    else:
        if (i < 0.9):
            return i - 0.733
        else:
            return 0.0
def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

clipAll = np.vectorize(clipDist)
while True:
    try:
        file = input()
    except EOFError:
        break
    # print("{}".format(file))
    x = file.split('/')
    if (x[1] == "progress.txt"):
        name = x[0]
        # print(name)
        array = np.loadtxt(file, delimiter='\t', skiprows=1)
        #Episode,EpReward,EpSteps,EpDist,EpAvgSpeed,DistHigh,AverageStepReward,StdStepReward,MaxStepReward,MinStepReward,AverageDeltaProg,StdDeltaProg,MaxDeltaProg,MinDeltaProg,TotalSteps,EpTime,TotalTime
        array[:, 3] = clipAll(array[:, 3])
        means = np.mean(array, axis=0)
        maxes = np.max(array, axis=0)
        stds = np.std(array, axis=0)
        tabs = '\t' * int(np.ceil((27 - len(name)) / 4))
        if (maxes[0] < 700):
            continue        
        f.write(name + ',' + tabs + "{:4.0f}".format(maxes[0]) + ',\t' + "{:8.4f}".format(means[3]*100) + ',' + "{:8.4f}".format(maxes[3]*100) + ',' + "{:8.4f}".format(means[4]) + ',' + "{:8.4f}".format(stds[3]) + '\n')
        #Name, Episodes, AvgDist, MaxDist, AvgSpeed, StdErr

        # Creating graphs
        fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
        average = 20
        half = int(average/2)
        halfmo = half - 1
        sli = slice(halfmo, -half)
        ax1.plot(array[:, 0], (array[:, 3]) * 100, ',-', color="#3385ff80")
        ax1.plot(array[:, 0][sli], moving_average((array[:, 3]*100), average), ',-', color="#1a75ffff")
        ax1.set_xlabel('Episode')
        ax1.set_ylabel('Track reached %')

        ax2.plot(array[:, 0], (array[:, 4]), ',-', color="#66ff9980")
        ax2.plot(array[:, 0][sli], moving_average((array[:, 4]), average), ',-', color="#4dff88ff")
        ax2.set_xlabel('Episode')
        ax2.set_ylabel('Average Speed\nkm/h')

        ax3.plot(array[:, 0], (array[:, 2]), ',-', color="#cc66ff80")
        ax3.plot(array[:, 0][sli], moving_average((array[:, 2]), average), ',-', color="#c44dffff")
        ax3.set_xlabel('Episode')
        ax3.set_ylabel('Episode reward')
        plt.savefig(name + '.png')
        fig.suptitle(name)
        plt.savefig(name + '-named.png')
        # plt.show()
        plt.close()
f.close()