import numpy as np

SELECTED = 0
# average = 0
# ideal = 1
# left = 2
# right = 3
#average(xyz),ideal(xyz),left(xyz),right(xyz),track_progress

def get_closest_spline_point(spline_points, x, y, selected = SELECTED):
    """
    Get the index of the closest point on the spline to the car.
    """
    # First, calculate the distance to every point on the center line
    distances = np.sqrt(
        (x - spline_points[:, 0+selected*3])**2 + (y - spline_points[:, 2+selected*3])**2)

    # Find the index of the point with the minimum distance
    index = np.argmin(distances)
    return index


def get_distance_to_center_line(spline_points, x, y, selected = SELECTED):
    """
    Calculate the distance from a coordinate to the center line.
    """
    # Get the closest point on the center line
    index = get_closest_spline_point(spline_points, x, y)

    # Calculate the distance to the closest point
    distance = np.sqrt((x - spline_points[:, 0+selected*3][index])**2 +
                       (y - spline_points[:, 2+selected*3][index])**2)

    # If the distance is smaller than 0.5, we consider it 0
    if distance <= 0.5:
        distance = 0.0
    return distance


def get_heading_error(spline_points, x, y, velocity_vector, selected = SELECTED):
    """
    Calculate the difference between the car heading (velocity) and the heading of the center line point closest to the car.
    """
    # Get the closest point on the center line
    index = get_closest_spline_point(spline_points, x, y)

    # Get the next spline point
    next_point = index + 15
    if next_point >= len(spline_points[:, 0+selected*3]):
        next_point = 0

    # Calculate the normalized direction vector from the closest spline point to the next point
    direction_vector = np.array(
        [spline_points[:, 0+selected*3][next_point] - x, spline_points[:, 2+selected*3][next_point] - y])
    direction_vector /= np.linalg.norm(direction_vector)

    # Normalize the velocity vector
    velocity_vector /= np.linalg.norm(velocity_vector)

    # Calculate the heading error
    heading_error = np.arccos(
        np.clip(np.dot(direction_vector, velocity_vector), -1.0, 1.0))
    return heading_error

def get_heading_error_2(spline_points, x, y, heading, selected = SELECTED):
    """
    Calculate the difference between the car heading (velocity) and the heading of the center line point closest to the car.
    """
    # Get the closest point on the center line
    index = get_closest_spline_point(spline_points, x, y)

    # Get the next spline point
    next = index + 15
    if next >= len(spline_points[:, 0+selected*3]):
        next = 0
    close_point = spline_points[index][[0+selected*3,2+selected*3]]
    next_point = spline_points[next][[0+selected*3,2+selected*3]]
    #average(xyz),ideal(xyz),left(xyz),right(xyz),track_progress

    distance_centerline = np.sqrt((x-close_point[0])**2 + (y-close_point[1])**2)
    distance_nextpoint = np.sqrt((x-next_point[0])**2 + (y-next_point[1])**2)
    # if (x>next_point[0])
    diffL_x = (next_point[0]-close_point[0])
    diffL_y = (next_point[1]-close_point[1])
    lineAngle = np.arctan(diffL_y/diffL_x) - (np.pi/2)
    if (lineAngle > np.pi):
        lineAngle -= np.pi*2
    diff_x = (next_point[0]-x)
    diff_y = (next_point[1]-y)
    angle_to_line = np.arctan(diff_y/diff_x) - (np.pi/2) #angle to line
    if (angle_to_line > np.pi):
        angle_to_line -= np.pi*2
    heading_error = angle_to_line - heading
    heading_error = (heading_error + np.pi) % (2 * np.pi) - np.pi

    # print("Heading: " + str(heading) + " Line Angle: " + str(lineAngle) + " angle: " + str(angle_to_line) + " Err: " + str(heading_error))
    # Calculate the heading error
    # heading_error = np.arccos(
    normalized_error = np.clip(heading_error/np.pi, -1.0, 1.0)
    # if (normalized_error < 0.05):
    #     normalized_error = 0
    return normalized_error

def get_heading_dist(spline_points, x, y, heading, index, selected = SELECTED):
    """
    Calculate the difference between the car heading (velocity) and the heading of the center line point closest to the car.
    """
    next = index +1
    if next >= len(spline_points[:, 0+selected*3]):
        next = 0
    close_point = spline_points[index][[0+selected*3,2+selected*3]]
    next_point = spline_points[next][[0+selected*3,2+selected*3]]
    #average(xyz),ideal(xyz),left(xyz),right(xyz),track_progress

    distance_nextpoint = np.sqrt((x-next_point[0])**2 + (y-next_point[1])**2)
    # if (x>next_point[0])
    diffL_x = (next_point[0]-close_point[0])
    diffL_y = (next_point[1]-close_point[1])
    lineAngle = np.arctan(diffL_y/diffL_x) - (np.pi/2)
    if (lineAngle > np.pi):
        lineAngle -= np.pi*2
    diff_x = (next_point[0]-x)
    diff_y = (next_point[1]-y)
    angle_to_line = np.arctan(diff_y/diff_x) - (np.pi/2) #angle to line
    if (angle_to_line > np.pi):
        angle_to_line -= np.pi*2
    heading_error = angle_to_line - heading
    heading_error = (heading_error + np.pi) % (2 * np.pi) - np.pi

    # print("Heading: " + str(heading) + " Line Angle: " + str(lineAngle) + " angle: " + str(angle_to_line) + " Err: " + str(heading_error))
    # Calculate the heading error
    # heading_error = np.arccos(
    normalized_error = np.clip(heading_error/np.pi, -1.0, 1.0)
    # if (normalized_error < 0.05):
    #     normalized_error = 0
    return normalized_error, distance_nextpoint