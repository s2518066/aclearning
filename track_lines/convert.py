import numpy as np
ideal_points = np.loadtxt(
        './ideal.csv', delimiter=',')
left_points = np.loadtxt(
    './left.csv', delimiter=',')
right_points = np.loadtxt(
    './right.csv', delimiter=',')
average_points = np.divide(left_points + right_points, 2)
fix_ideal = np.dstack([ideal_points[:,0],ideal_points[:,1],0-ideal_points[:,2],ideal_points[:,3]])[0]
fix_left = np.dstack([left_points[:,0],left_points[:,1],0-left_points[:,2],left_points[:,3]])[0]
fix_right = np.dstack([right_points[:,0],right_points[:,1],0-right_points[:,2],right_points[:,3]])[0]
fix_avg = np.dstack([average_points[:,0],average_points[:,1],0-average_points[:,2],average_points[:,3]])[0]
combined_points = np.hstack([fix_avg[:,[0,1,2]], fix_ideal[:,[0,1,2]], fix_left[:,[0,1,2]], fix_right])
np.savetxt('./average.csv', average_points, delimiter=',')
print(combined_points.shape)
np.savetxt('./combined.csv', combined_points, delimiter=',')